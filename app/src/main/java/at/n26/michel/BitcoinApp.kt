package at.n26.michel

import android.app.Application
import at.n26.michel.di.application.DaggerApplicationComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class BitcoinApp : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        DaggerApplicationComponent.create().inject(this)
        AndroidThreeTen.init(this)
    }

    override fun androidInjector(): AndroidInjector<Any> =
        androidInjector
}