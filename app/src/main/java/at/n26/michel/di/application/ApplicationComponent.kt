package at.n26.michel.di.application

import at.n26.michel.BitcoinApp
import at.n26.michel.di.ViewModelModule
import at.n26.michel.di.activity.ActivityBuildersModule
import at.n26.michel.di.network.NetworkModule
import dagger.Component
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityBuildersModule::class
    ]
)
interface ApplicationComponent {

    fun inject(app: BitcoinApp)
}

@Module(includes = [ViewModelModule::class, NetworkModule::class])
class AppModule