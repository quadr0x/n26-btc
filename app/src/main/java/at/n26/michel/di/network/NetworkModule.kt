package at.n26.michel.di.network

import at.n26.michel.BuildConfig
import at.n26.michel.remote.BaseUrl
import at.n26.michel.remote.BitcoinApi
import at.n26.michel.remote.BitcoinClient
import at.n26.michel.remote.RealBitcoinClient
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideBitcoinApi(
        okHttpClient: OkHttpClient,
        gson: Gson,
        apiUrl: BaseUrl
    ): BitcoinApi =
        Retrofit.Builder().apply {
            baseUrl(apiUrl.url)
            client(okHttpClient)
            addConverterFactory(GsonConverterFactory.create(gson))
            addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        }
            .build()
            .create(BitcoinApi::class.java)

    @Provides
    @Singleton
    fun provideBitcoinClient(api: BitcoinApi): BitcoinClient =
        RealBitcoinClient(api)

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient()
        .newBuilder()
        .apply {
            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
                addInterceptor(loggingInterceptor)
            }
        }
        .build()

    @Provides
    @Singleton
    fun provideGson(): Gson = Gson()

    @Provides
    @Singleton
    fun provideBaseUrl(): BaseUrl =
        BaseUrl("https://api.blockchain.info/")

}