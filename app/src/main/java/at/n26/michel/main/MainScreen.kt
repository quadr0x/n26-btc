package at.n26.michel.main

import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import at.n26.michel.R
import at.n26.michel.base.BaseActivity
import at.n26.michel.base.BaseViewModel
import at.n26.michel.remote.BitcoinClient
import at.n26.michel.remote.ChartData
import at.n26.michel.remote.Stats
import at.n26.michel.remote.TimespanMode
import at.n26.michel.util.buttonChanges
import at.n26.michel.util.setup
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.jakewharton.rxrelay2.PublishRelay
import dagger.android.AndroidInjection
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.Singles
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

class MainActivity : BaseActivity(R.layout.activity_main), SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
    }

    override fun onResume() {
        super.onResume()
        bind()

        srlRoot.setOnRefreshListener(this)
        chart.setup(this)

        viewModel.loadBitcoinData()
    }

    override fun bind() {
        viewModel.dataRelay
            .subscribe { item ->
                when (item) {
                    is MainViewModel.MainState.Loading -> {
                        srlRoot.isRefreshing = item.loading
                    }
                    is MainViewModel.MainState.Error -> {

                    }
                    is MainViewModel.MainState.DataLoaded -> {
                        tvValue.text = String.format(
                            getString(R.string.main_btc_value),
                            item.stats.marketPriceUsd
                        )
                        tvLastUpdateValue.text = LocalDateTime.now()
                            .format(DateTimeFormatter.ofPattern("dd.MM.YYYY HH:mm:ss"))

                        val chartColorRes = when (item.timespanMode) {
                            TimespanMode.Week -> R.color.n26Petrol
                            TimespanMode.Month -> R.color.n26Rhubarb
                            TimespanMode.Year -> R.color.n26Wheat
                            TimespanMode.AllTime -> R.color.n26Teal
                        }

                        item.chartData.values
                            .map { Entry(it.x.toFloat(), it.y.toFloat()) }
                            .let {
                                LineDataSet(it, "Label").apply {
                                    color =
                                        ContextCompat.getColor(this@MainActivity, chartColorRes)
                                    fillColor =
                                        ContextCompat.getColor(this@MainActivity, chartColorRes)
                                    mode = LineDataSet.Mode.CUBIC_BEZIER
                                    fillAlpha = 255
                                    setDrawFilled(true)
                                    setDrawValues(false)
                                    setDrawCircles(false)
                                    setDrawHighlightIndicators(false)
                                }
                            }
                            .let { LineData(it) }
                            .let { lineData ->
                                chart.apply {
                                    data = lineData
                                    invalidate()
                                }
                            }
                    }
                }
            }
            .addTo(disposable)

        btnGroup.buttonChanges()
            .distinctUntilChanged()
            .subscribe { viewId ->
                when (viewId) {
                    R.id.btnWeek -> viewModel.loadBitcoinData(TimespanMode.Week)
                    R.id.btnMonth -> viewModel.loadBitcoinData(TimespanMode.Month)
                    R.id.btnYear -> viewModel.loadBitcoinData(TimespanMode.Year)
                    R.id.btnAllTime -> viewModel.loadBitcoinData(TimespanMode.AllTime)
                }
            }
            .addTo(disposable)
    }

    override fun onRefresh() {
        viewModel.loadBitcoinData()
    }

    override fun onPause() {
        teardownViews()
        super.onPause()
    }

    private fun teardownViews() {
        srlRoot.setOnRefreshListener(null)
        btnGroup.clearOnButtonCheckedListeners()
    }
}

class MainViewModel @Inject constructor(
    private val client: BitcoinClient
) : BaseViewModel() {

    val dataRelay = PublishRelay.create<MainState>()

    fun loadBitcoinData(timespanMode: TimespanMode = TimespanMode.Week) {
        Singles.zip(loadStats(), loadChartData(timespanMode))
            .map { MainState.DataLoaded(it.first, it.second, timespanMode) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { dataRelay.accept(MainState.Loading(true)) }
            .doAfterTerminate { dataRelay.accept(MainState.Loading(false)) }
            .subscribe(dataRelay::accept) {
                Log.e("API", "Stats could not be loaded", it)
                dataRelay.accept(MainState.Error(it))
            }
            .addTo(compositeDisposable)
    }

    private fun loadStats(): Single<Stats> =
        client.loadStats()

    private fun loadChartData(timespanMode: TimespanMode): Single<ChartData> =
        client.loadChartData(timespanMode)

    sealed class MainState {
        data class Loading(val loading: Boolean) : MainState()
        data class Error(val error: Throwable) : MainState()
        data class DataLoaded(
            val stats: Stats,
            val chartData: ChartData,
            val timespanMode: TimespanMode
        ) : MainState()
    }
}