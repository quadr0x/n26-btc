package at.n26.michel.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BitcoinApi {

    @GET("stats")
    fun loadStats(): Single<Stats>

    @GET("charts/market-price")
    fun loadChartData(@Query("timespan") timespan: String): Single<ChartData>
}