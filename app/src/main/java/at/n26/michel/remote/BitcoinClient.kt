package at.n26.michel.remote

import io.reactivex.Single

interface BitcoinClient {

    fun loadStats(): Single<Stats>

    fun loadChartData(timespanMode: TimespanMode): Single<ChartData>
}

class RealBitcoinClient(private val api: BitcoinApi) : BitcoinClient {

    override fun loadStats(): Single<Stats> =
        api.loadStats()

    override fun loadChartData(timespanMode: TimespanMode): Single<ChartData> =
        api.loadChartData(timespanMode.combine())
}