package at.n26.michel.remote

import com.google.gson.annotations.SerializedName


data class ChartData(
    @SerializedName("description")
    val description: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("period")
    val period: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("unit")
    val unit: String,
    @SerializedName("values")
    val values: List<Value>
) {
    data class Value(
        @SerializedName("x")
        val x: Int,
        @SerializedName("y")
        val y: Double
    )
}

sealed class TimespanMode(private val unit: String, private val value: Int) {

    object AllTime : TimespanMode("years", 100)
    object Year : TimespanMode("years", 1)
    object Month : TimespanMode("months", 1)
    object Week : TimespanMode("weeks", 1)

    fun combine(): String = value.toString() + "" + unit
}