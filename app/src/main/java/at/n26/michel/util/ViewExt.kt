package at.n26.michel.util

import android.content.Context
import androidx.core.content.ContextCompat
import at.n26.michel.R
import com.github.mikephil.charting.charts.LineChart
import com.google.android.material.button.MaterialButtonToggleGroup
import io.reactivex.Observable

fun MaterialButtonToggleGroup.buttonChanges(): Observable<Int> =
    Observable.create { observable ->
        this.addOnButtonCheckedListener { _, checkedId, isChecked ->
            if (isChecked) {
                observable.onNext(checkedId)
            }
        }
    }

fun LineChart.setup(context: Context) =
    apply {
        description = null
        legend.isEnabled = false
        axisRight.isEnabled = false
        xAxis.isEnabled = false
        isDoubleTapToZoomEnabled = false
        setTouchEnabled(false)
        setNoDataTextColor(
            ContextCompat.getColor(
                context,
                R.color.colorPrimary
            )
        )
    }