package at.n26.michel

import android.util.Log
import androidx.annotation.CallSuper
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.Before

abstract class BaseTest {

    @CallSuper
    @Before
    open fun setup() {
        MockKAnnotations.init(this)

        mockkStatic(Log::class)
        every { Log.v(any(), any(), any()) } returns 0
        every { Log.d(any(), any(), any()) } returns 0
        every { Log.i(any(), any(), any()) } returns 0
        every { Log.e(any(), any(), any()) } returns 0
    }
}