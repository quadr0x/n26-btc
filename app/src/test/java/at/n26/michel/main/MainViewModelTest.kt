package at.n26.michel.main

import at.n26.michel.BaseTest
import at.n26.michel.RxImmediateSchedulerRule
import at.n26.michel.remote.BitcoinClient
import at.n26.michel.remote.ChartData
import at.n26.michel.remote.Stats
import at.n26.michel.remote.TimespanMode
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class MainViewModelTest : BaseTest() {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @MockK
    lateinit var client: BitcoinClient

    private lateinit var viewModel: MainViewModel

    override fun setup() {
        super.setup()

        viewModel = MainViewModel(client)
    }

    @Test
    fun `successfully load data`() {
        every { client.loadStats() } returns Single.just(DEFAULT_STATS)
        every { client.loadChartData(any()) } returns Single.just(DEFAULT_CHART_DATA)

        val testObserver = viewModel.dataRelay.test()
        val stateValues = testObserver.values()

        viewModel.loadBitcoinData()

        Assert.assertEquals(MainViewModel.MainState.Loading(true), stateValues[0])
        Assert.assertEquals(
            MainViewModel.MainState.DataLoaded(
                DEFAULT_STATS,
                DEFAULT_CHART_DATA,
                TimespanMode.Week
            ),
            stateValues[1]
        )
        Assert.assertEquals(MainViewModel.MainState.Loading(false), stateValues[2])
    }

    @Test
    fun `loading stats failed`() {
        val throwable = Throwable("No connection")
        every { client.loadStats() } returns Single.error(throwable)
        every { client.loadChartData(any()) } returns Single.just(DEFAULT_CHART_DATA)

        val testObserver = viewModel.dataRelay.test()
        val stateValues = testObserver.values()

        viewModel.loadBitcoinData()

        Assert.assertEquals(MainViewModel.MainState.Loading(true), stateValues[0])
        Assert.assertEquals(MainViewModel.MainState.Error(throwable), stateValues[1])
        Assert.assertEquals(MainViewModel.MainState.Loading(false), stateValues[2])
    }

    @Test
    fun `loading chart data failed`() {
        val throwable = Throwable("No connection")
        every { client.loadStats() } returns Single.just(DEFAULT_STATS)
        every { client.loadChartData(any()) } returns Single.error(throwable)

        val testObserver = viewModel.dataRelay.test()
        val stateValues = testObserver.values()

        viewModel.loadBitcoinData()

        Assert.assertEquals(MainViewModel.MainState.Loading(true), stateValues[0])
        Assert.assertEquals(MainViewModel.MainState.Error(throwable), stateValues[1])
        Assert.assertEquals(MainViewModel.MainState.Loading(false), stateValues[2])
    }

    companion object {
        val DEFAULT_STATS = Stats(
            blocksSize = 1000,
            difficulty = 10,
            estimatedBtcSent = 1000,
            estimatedTransactionVolumeUsd = 1000.00,
            hashRate = 1000.00,
            marketPriceUsd = 5000.00,
            minersRevenueBtc = 20,
            minersRevenueUsd = 20.00,
            minutesBetweenBlocks = 20.00,
            nBlocksMined = 1000,
            nBlocksTotal = 2000,
            nBtcMined = 2000,
            nTx = 1000,
            nextretarget = 1000,
            timestamp = 1234312312.0,
            totalBtcSent = 2000,
            totalFeesBtc = 3000,
            totalbc = 300,
            tradeVolumeBtc = 30.0,
            tradeVolumeUsd = 300.0
        )

        val DEFAULT_CHART_DATA = ChartData(
            description = "Test",
            name = "TestName",
            period = "day",
            status = "ok",
            unit = "Dollar",
            values = listOf()
        )
    }

}